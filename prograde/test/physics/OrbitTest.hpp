/*
    Copyright (C) 2018 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef ORBITTEST_H
#define ORBITTEST_H

#include "physics/Orbit.hpp"
#include <QtTest>

class OrbitTest : public QObject
{
	Q_OBJECT
  private slots:
	void ConstructFromPosVelWithNullEccentricity()
	{
		Orbit::MassiveBodyMass m(5.97237e+24);
		UniversalTime ut(511920000.588);

		Vector3 pos(6368729.2405517250299453735351562,
		            227324.25792485033161938190460205,
		            2760801.614078846760094165802002),
		    vel(-227.51190828843942881576367653906,
		        7571.7634536776449749595485627651,
		        -98.624893585595344802641193382442);

		Orbit orbit(m, pos, vel, ut);
		// semiMajorAxis can become NaN under these conditions
		// if computed eccentricity becomes Exactly 0.0
		QVERIFY(orbit.getParameters().semiMajorAxis
		        == orbit.getParameters().semiMajorAxis);
	}
};

#endif // ORBITTEST_H
