#include "MainWin.hpp"

#include <QCheckBox>
#include <QDoubleSpinBox>
#include <random>

#include "LibPlanet.hpp"
#include "LibTerrain.hpp"
#include "graphics/renderers/SpacecraftRenderer.hpp"
#include "graphics/renderers/StarRenderer.hpp"
#include "graphics/renderers/planet/PlanetRenderer.hpp"
#include "physics/CSVOrbit.hpp"

MainWin::MainWin()

{
	const QString planetsystemdir(
	    QSettings().value("simulation/planetsystemdir").toString());

	unsigned int tries(
	    QSettings().value("simulation/randomsystem").toBool() ? 3 : 1);

	std::mt19937 rng(
	    std::chrono::high_resolution_clock::now().time_since_epoch().count());

	while(tries > 0)
	{
		tries--;
		QFile jsonFile;
		if(QSettings().value("simulation/randomsystem").toBool())
		{
			QStringList nameFilter;
			nameFilter << "*.json";

			QStringList files;
			std::uniform_int_distribution<std::size_t> dist(0,
			                                                files.size() - 1);
			QDirIterator it(planetsystemdir, QStringList() << "*.json",
			                QDir::Files, QDirIterator::Subdirectories);
			while(it.hasNext())
			{
				files << it.next();
			}

			jsonFile.setFileName(files[dist(rng)]);
		}
		else
		{
			jsonFile.setFileName(planetsystemdir + "/definition.json");
		}

		if(jsonFile.exists())
		{
			PlanetRenderer::currentSystemDir() = planetsystemdir;
			CSVOrbit::currentSystemDir()       = planetsystemdir;

			jsonFile.open(QIODevice::ReadOnly);
			const QJsonDocument jsonDoc
			    = QJsonDocument::fromJson(jsonFile.readAll());
			const QString name(QFileInfo(jsonFile).dir().dirName());
			orbitalSystem = std::make_unique<OrbitalSystem>(name.toStdString(),
			                                                jsonDoc.object());
			if(!orbitalSystem->isValid())
			{
				qWarning() << orbitalSystem->getName().c_str()
				           << " is invalid... ";
				orbitalSystem.reset();
				if(tries > 0)
				{
					qWarning() << "Trying another one...";
				}
				else
				{
					qCritical() << "All tries done. Shuting down...";
				}
			}
			else
			{
				tries = 0;
			}
		}
		else
		{
			qCritical() << tr("The planetary system root directory doesn't "
			                  "contain any definition.json file.");
		}
	}

	auto barycenters = orbitalSystem->getAllBinariesNames();
	auto stars       = orbitalSystem->getAllStarsNames();
	auto fcPlanets   = orbitalSystem->getAllFirstClassPlanetsNames();
	auto satellites  = orbitalSystem->getAllSatellitePlanetsNames();
	auto spacecrafts = orbitalSystem->getAllSpacecraftsNames();

	std::cout << "-=-=- SYSTEM " << orbitalSystem->getName() << " -=-=-"
	          << std::endl;
	std::cout << "Barycenters : " << barycenters.size() << std::endl;
	for(auto const& name : barycenters)
	{
		std::cout << name << std::endl;
	}
	std::cout << std::endl;

	std::cout << "Stars : " << stars.size() << std::endl;
	for(auto const& name : stars)
	{
		std::cout << name << std::endl;
	}
	std::cout << std::endl;

	std::cout << "Main Planets : " << fcPlanets.size() << std::endl;
	for(auto const& name : fcPlanets)
	{
		std::cout << name << std::endl;
	}
	std::cout << std::endl;

	std::cout << "Satellites : " << satellites.size() << std::endl;
	for(auto const& name : satellites)
	{
		std::cout << name << "("
		          << (*orbitalSystem)[name]->getParent()->getName() << ")"
		          << std::endl;
	}
	std::cout << std::endl;

	std::cout << "Spacecrafts : " << spacecrafts.size() << std::endl;
	for(auto const& name : spacecrafts)
	{
		std::cout << name << "("
		          << (*orbitalSystem)[name]->getParent()->getName() << ")"
		          << std::endl;
	}
	std::cout << std::endl;
}

void MainWin::selectOrbitable(QTreeWidgetItem* item, int column)
{
	auto& cam(renderer.getCamera<OrbitalSystemCamera>("planet"));

	QString name(item->text(column));
	if(name.contains('('))
	{
		const unsigned int pos(name.lastIndexOf('('));
		name = name.left(pos - 1);
	}
	auto const* orbitable = (*orbitalSystem)[name.toLatin1().data()];

	if(orbitable->getOrbit() != nullptr
	   && !orbitable->getOrbit()->isInRange(clock.getCurrentUt()))
	{
		if(orbitable->getOrbitableType() == Orbitable::Type::SPACECRAFT)
		{
			item->setForeground(0, QColor("red"));
		}
		return;
	}
	if(orbitable->getOrbitableType() == Orbitable::Type::SPACECRAFT)
	{
		item->setForeground(0, QColor("green"));
	}

	cam.target            = orbitable;
	auto const* camTarget = dynamic_cast<CelestialBody const*>(orbitable);

	if(cam.relativePosition.length() >= camTarget->getSphereOfInfluenceRadius())
	{
		cam.relativePosition = cam.relativePosition.getUnitForm()
		                       * camTarget->getSphereOfInfluenceRadius() / 2.0;
	}

	if(cam.relativePosition.length()
	   <= camTarget->getCelestialBodyParameters().radius)
	{
		cam.relativePosition = cam.relativePosition.getUnitForm()
		                       * camTarget->getCelestialBodyParameters().radius
		                       * 2.0;
	}
}

void MainWin::actionEvent(BaseInputManager::Action const& a, bool pressed)
{
	if(pressed)
	{
		auto& cam(renderer.getCamera<OrbitalSystemCamera>("planet"));
		auto const* camTarget = dynamic_cast<CelestialBody const*>(cam.target);
		if(a.id == "nextcelestialbody")
		{
			std::vector<CelestialBody*> bodies(
			    orbitalSystem->getAllCelestialBodiesPointers());
			bodyTracked++;
			if(bodyTracked == bodies.size())
			{
				bodyTracked = 0;
			}
			cam.target = bodies[bodyTracked];

			if(cam.target->getOrbit() != nullptr
			   && !cam.target->getOrbit()->isInRange(clock.getCurrentUt()))
			{
				actionEvent({"nextcelestialbody", ""}, true);
			}

			if(cam.relativePosition.length()
			   >= camTarget->getSphereOfInfluenceRadius())
			{
				cam.relativePosition = cam.relativePosition.getUnitForm()
				                       * camTarget->getSphereOfInfluenceRadius()
				                       / 2.0;
			}

			if(cam.relativePosition.length()
			   <= camTarget->getCelestialBodyParameters().radius)
			{
				cam.relativePosition
				    = cam.relativePosition.getUnitForm()
				      * camTarget->getCelestialBodyParameters().radius * 2.0;
			}
		}
		else if(a.id == "prevcelestialbody")
		{
			std::vector<CelestialBody*> bodies(
			    orbitalSystem->getAllCelestialBodiesPointers());
			if(bodyTracked > 0)
			{
				bodyTracked--;
			}
			else
			{
				bodyTracked = bodies.size() - 1;
			}
			cam.target = bodies[bodyTracked];

			if(cam.target->getOrbit() != nullptr
			   && !cam.target->getOrbit()->isInRange(clock.getCurrentUt()))
			{
				actionEvent({"prevcelestialbody", ""}, true);
			}

			if(cam.relativePosition.length()
			   >= camTarget->getSphereOfInfluenceRadius())
			{
				cam.relativePosition = Vector3(
				    camTarget->getSphereOfInfluenceRadius() / 2.0, 0.0, 0.0);
			}

			if(cam.relativePosition.length()
			   <= camTarget->getCelestialBodyParameters().radius)
			{
				cam.relativePosition
				    = cam.relativePosition.getUnitForm()
				      * camTarget->getCelestialBodyParameters().radius * 2.0;
			}
		}
		else if(a.id == "timecoeffdown")
		{
			const float tc(clock.getTimeCoeff());
			if(tc > 1.f && !clock.getLockedRealTime())
			{
				clock.setTimeCoeff(tc / 10.f);
			}
		}
		else if(a.id == "timecoeffup")
		{
			const float tc(clock.getTimeCoeff());
			if(tc < 10000000.f && !clock.getLockedRealTime())
			{
				clock.setTimeCoeff(tc * 10.f);
			}
		}
		else if(a.id == "resetvrpos")
		{
			if(vrHandler->isEnabled())
			{
				vrHandler->resetPos();
			}
		}
		else if(a.id == "togglelabels")
		{
			if(CelestialBodyRenderer::renderLabels() > 0.f)
			{
				CelestialBodyRenderer::renderLabels() = 0.f;
			}
			else
			{
				CelestialBodyRenderer::renderLabels() = 1.f;
			}
		}
		else if(a.id == "toggleorbits")
		{
			if(CelestialBodyRenderer::renderOrbits() > 0.f)
			{
				CelestialBodyRenderer::renderOrbits() = 0.f;
			}
			else
			{
				CelestialBodyRenderer::renderOrbits() = 1.f;
			}
		}
		else if(a.id == "togglelock")
		{
			cam.toggleLockedOnRotation(clock.getCurrentUt());
		}
		else if(a.id == "toggleGI")
		{
			SpacecraftRenderer::globalIllumination
			    = !SpacecraftRenderer::globalIllumination;
		}
		// CONTROLS
		else if(a.id == "centercam")
		{
			const Vector3 unitRelPos(cam.relativePosition.getUnitForm());
			const float yaw(atan2(unitRelPos[1], unitRelPos[0]));
			const float pitch(cam.pitch = -1.0 * asin(unitRelPos[2]));
			cam.yaw   = yaw;
			cam.pitch = pitch;
		}
		else if(a.id == "forward")
		{
			negativeVelocity.setZ(-1);
		}
		else if(a.id == "left")
		{
			negativeVelocity.setX(-1);
		}
		else if(a.id == "backward")
		{
			positiveVelocity.setZ(1);
		}
		else if(a.id == "right")
		{
			positiveVelocity.setX(1);
		}
	}
	else
	{
		// CONTROLS
		if(a.id == "forward")
		{
			negativeVelocity.setZ(0);
		}
		else if(a.id == "left")
		{
			negativeVelocity.setX(0);
		}
		else if(a.id == "backward")
		{
			positiveVelocity.setZ(0);
		}
		else if(a.id == "right")
		{
			positiveVelocity.setX(0);
		}
	}
	AbstractMainWin::actionEvent(a, pressed);
}

bool MainWin::event(QEvent* e)
{
	if(e->type() == QEvent::Type::Close)
	{
		dialog->close();
		telescopeDialog->close();
	}
	return AbstractMainWin::event(e);
}

void MainWin::mousePressEvent(QMouseEvent* e)
{
	if(e->button() == Qt::MouseButton::LeftButton)
	{
		rotateViewEnabled = true;
		QCursor::setPos(x() + width() / 2, y() + height() / 2);
	}
	else if(e->button() == Qt::MouseButton::RightButton)
	{
		trackballEnabled = true;
		QCursor::setPos(x() + width() / 2, y() + height() / 2);
	}
	if(rotateViewEnabled && trackballEnabled)
	{
		OrbitalSystemRenderer::autoCameraTarget = false;
	}
}

void MainWin::mouseReleaseEvent(QMouseEvent* e)
{
	if(e->button() == Qt::MouseButton::LeftButton)
	{
		rotateViewEnabled = false;
	}
	else if(e->button() == Qt::MouseButton::RightButton)
	{
		trackballEnabled = false;
	}
	OrbitalSystemRenderer::autoCameraTarget = true;
}

void MainWin::mouseMoveEvent(QMouseEvent* e)
{
	if(!rotateViewEnabled && !trackballEnabled)
	{
		return;
	}

	auto& cam(renderer.getCamera<OrbitalSystemCamera>("planet"));
	const float dx
	    = (x() + static_cast<float>(width()) / 2 - e->globalX()) / width();
	const float dy
	    = (y() + static_cast<float>(height()) / 2 - e->globalY()) / height();

	// means both mouse buttons are clicked
	if(rotateViewEnabled && trackballEnabled)
	{
		double alt(cam.getAltitude());
		alt *= 1.f - dy;
		cam.setAltitude(alt);
	}
	else if(rotateViewEnabled || trackballEnabled)
	{
		double dYaw(dx * constant::pi / 3.0), dPitch(dy * constant::pi / 3.0);
		double coeff(1.0);
		if(trackballEnabled)
		{
			coeff = cam.getAltitude()
			        / dynamic_cast<CelestialBody const*>(cam.target)
			              ->getCelestialBodyParameters()
			              .radius;
		}
		else
		{
			coeff = getVerticalFOV() / 60.0;
		}
		coeff = coeff > 1.0 ? 1.0 : coeff;
		dPitch *= coeff;
		dYaw *= coeff;

		if(cam.pitch + dPitch > M_PI_2 - 0.20)
		{
			dPitch = M_PI_2 - 0.20 - cam.pitch;
		}
		if(cam.pitch + dPitch < -1.f * M_PI_2 + 0.2)
		{
			dPitch = -1.f * M_PI_2 + 0.2 - cam.pitch;
		}
		cam.yaw += dYaw;
		cam.pitch += dPitch;
		if(trackballEnabled)
		{
			cam.relativePosition.rotateAlongZ(dYaw);
			const double posPitch(asin(cam.relativePosition.getUnitForm()[2]));
			if(dPitch - posPitch < -1.0 * M_PI_2 + 0.01)
			{
				dPitch = -1.0 * M_PI_2 + 0.01 + posPitch;
			}
			else if(dPitch - posPitch > M_PI_2 - 0.01)
			{
				dPitch = M_PI_2 - 0.01 + posPitch;
			}

			const Vector3 axis(
			    crossProduct(Vector3(0.0, 0.0, 1.0), cam.relativePosition));
			const Matrix4x4 rot(dPitch, axis);
			cam.relativePosition = rot * cam.relativePosition;
		}
	}
	QCursor::setPos(x() + width() / 2, y() + height() / 2);
}

void MainWin::wheelEvent(QWheelEvent* e)
{
	velMag *= 1.f + e->angleDelta().y() / 1000.f;

	AbstractMainWin::wheelEvent(e);
}

void MainWin::vrEvent(VRHandler::Event const& e)
{
	auto const* cam(
	    dynamic_cast<OrbitalSystemCamera*>(&renderer.getCamera("planet")));
	switch(e.type)
	{
		case VRHandler::EventType::BUTTON_PRESSED:
			switch(e.button)
			{
				case VRHandler::Button::GRIP:
				{
					OrbitalSystemRenderer::autoCameraTarget = false;
					Controller const* left(
					    vrHandler->getController(Side::LEFT));
					Controller const* right(
					    vrHandler->getController(Side::RIGHT));
					if(e.side == Side::LEFT && left != nullptr)
					{
						leftGripPressed = true;
						initControllerRelPos
						    = Utils::fromQt(
						          renderer.getCamera("planet")
						              .seatedTrackedSpaceToWorldTransform()
						          * left->getPosition())
						          / CelestialBodyRenderer::overridenScale()
						      + cam->relativePosition;
					}
					else if(e.side == Side::RIGHT && right != nullptr)
					{
						rightGripPressed = true;
						initControllerRelPos
						    = Utils::fromQt(
						          renderer.getCamera("planet")
						              .seatedTrackedSpaceToWorldTransform()
						          * right->getPosition())
						          / CelestialBodyRenderer::overridenScale()
						      + cam->relativePosition;
					}
					else
					{
						break;
					}
					if(leftGripPressed && rightGripPressed && left != nullptr
					   && right != nullptr)
					{
						initControllersDistance
						    = left->getPosition().distanceToPoint(
						        right->getPosition());
						initScale = CelestialBodyRenderer::overridenScale();

						QVector3D controllersMidPoint;
						controllersMidPoint
						    = left->getPosition() + right->getPosition();
						controllersMidPoint /= 2.f;

						controllersMidPoint
						    = renderer.getCamera("planet")
						          .seatedTrackedSpaceToWorldTransform()
						      * controllersMidPoint;
						scaleCenter
						    = Utils::fromQt(controllersMidPoint)
						          / CelestialBodyRenderer::overridenScale()
						      + cam->relativePosition;
					}
					break;
				}
				case VRHandler::Button::TRIGGER:
				{
					if(CelestialBodyRenderer::renderLabels() > 0.f)
					{
						CelestialBodyRenderer::renderLabels() = 0.f;
					}
					else
					{
						CelestialBodyRenderer::renderLabels() = 1.f;
					}
					if(CelestialBodyRenderer::renderOrbits() > 0.f)
					{
						CelestialBodyRenderer::renderOrbits() = 0.f;
					}
					else
					{
						CelestialBodyRenderer::renderOrbits() = 1.f;
					}
					break;
				}
				case VRHandler::Button::TOUCHPAD:
				{
					Controller const* ctrl(vrHandler->getController(e.side));
					if(ctrl != nullptr)
					{
						QVector2D padCoords(ctrl->getPadCoords());
						if(fabsf(padCoords[0])
						   < fabsf(padCoords[1])) // UP OR DOWN
						{
							const float tc(clock.getTimeCoeff());
							if(padCoords[1] < 0.0f) // DOWN
							{
								if(tc > 1.f && !clock.getLockedRealTime())
								{
									clock.setTimeCoeff(tc / 10.f);
								}
							}
							else // UP
							{
								if(tc < 10000000.f
								   && !clock.getLockedRealTime())
								{
									clock.setTimeCoeff(tc * 10.f);
								}
							}
						}
					}
					break;
				}
				default:
					break;
			}
			break;
		case VRHandler::EventType::BUTTON_UNPRESSED:
			switch(e.button)
			{
				case VRHandler::Button::GRIP:
				{
					Controller const* left(
					    vrHandler->getController(Side::LEFT));
					Controller const* right(
					    vrHandler->getController(Side::RIGHT));
					if(e.side == Side::LEFT)
					{
						leftGripPressed = false;
						if(right != nullptr && rightGripPressed)
						{
							initControllerRelPos
							    = Utils::fromQt(
							          renderer.getCamera("planet")
							              .seatedTrackedSpaceToWorldTransform()
							          * right->getPosition())
							          / CelestialBodyRenderer::overridenScale()
							      + cam->relativePosition;
						}
						else
						{
							OrbitalSystemRenderer::autoCameraTarget = true;
						}
					}
					else if(e.side == Side::RIGHT)
					{
						rightGripPressed = false;
						if(left != nullptr && leftGripPressed)
						{
							initControllerRelPos
							    = Utils::fromQt(
							          renderer.getCamera("planet")
							              .seatedTrackedSpaceToWorldTransform()
							          * left->getPosition())
							          / CelestialBodyRenderer::overridenScale()
							      + cam->relativePosition;
						}
						else
						{
							OrbitalSystemRenderer::autoCameraTarget = true;
						}
					}
					break;
				}
				default:
					break;
			}
			break;
		default:
			break;
	}

	AbstractMainWin::vrEvent(e);
}

void MainWin::initLibraries()
{
	initLibrary<trn::LibTerrain>();
	initLibrary<LibPlanet>();
}

void MainWin::initScene()
{
	QCursor c(cursor());
	c.setShape(Qt::CursorShape::BlankCursor);
	setCursor(c);

	clock.setTargetFPS(0.f);
	stars = std::make_unique<StarryBackground>();
	stars->initFromFile(23.4392811 * constant::pi / 180.f);

	auto cam = std::make_unique<OrbitalSystemCamera>(
	    *vrHandler, toneMappingModel->exposure, toneMappingModel->dynamicrange);
	cam->setPerspectiveProj(renderer.getVerticalFOV(),
	                        renderer.getAspectRatioFromFOV());

	// use ISS as target if in the system
	cam->target = orbitalSystem->getAllCelestialBodiesPointers()[0];
	for(auto const& name : orbitalSystem->getAllSpacecraftsNames())
	{
		if(name == "ISS")
		{
			cam->target = dynamic_cast<CelestialBody*>((*orbitalSystem)[name]);
		}
	}

	cam->relativePosition
	    = Vector3(dynamic_cast<CelestialBody const*>(cam->target)
	                      ->getCelestialBodyParameters()
	                      .radius
	                  * 2.0,
	              0.0, 0.0);
	systemRenderer = std::make_unique<OrbitalSystemRenderer>(*orbitalSystem);

	renderer.removeSceneRenderPath("default");
	renderer.appendSceneRenderPath("planet",
	                               Renderer::RenderPath(std::move(cam)));

	CelestialBodyRenderer::overridenScale() = 1.0;

	// we will draw them ourselves
	renderer.pathIdRenderingControllers = "";

	auto* tools(menuBar->addMenu("Tools"));
	tools->addAction("Orbitables List", this,
	                 [this]() { this->dialog->show(); });
	tools->addAction(tr("Telescope mode"), this,
	                 [this]() { this->telescopeDialog->show(); });

	dialog = std::make_unique<QDialog>();
	dialog->setFixedSize(250, 600);
	dialog->setWindowTitle("Orbitables List");

	auto* layout = make_qt_unique<QVBoxLayout>(*dialog);
	tree         = make_qt_unique<QTreeWidget>(*dialog);
	tree->setColumnCount(1);
	tree->setHeaderLabel(QString());
	connect(tree, &QTreeWidget::itemActivated, this, &MainWin::selectOrbitable);
	layout->addWidget(tree);
	constructItems(orbitalSystem->getRootOrbitable());

	telescopeDialog = std::make_unique<QDialog>();
	telescopeDialog->setWindowTitle(tr("Telescope mode"));

	auto* layoutF = make_qt_unique<QFormLayout>(*telescopeDialog);

	auto* cb = make_qt_unique<QCheckBox>(*telescopeDialog);
	cb->setChecked(telescope.active);
	connect(cb, &QCheckBox::stateChanged,
	        [this](int state)
	        {
		        telescope.active = (state == Qt::Checked);
		        updateFOVFromTelescope();
	        });
	layoutF->addRow(tr("Active"), cb);

	auto* spin = make_qt_unique<QSpinBox>(*telescopeDialog);
	spin->setMinimum(0);
	spin->setMaximum(10000);
	spin->setValue(telescope.diameter);
	connect(spin, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
	        [this](int val)
	        {
		        telescope.diameter = val;
		        updateFOVFromTelescope();
	        });
	layoutF->addRow(tr("Diameter (mm)"), spin);

	spin = make_qt_unique<QSpinBox>(*telescopeDialog);
	spin->setMinimum(0);
	spin->setMaximum(100000);
	spin->setValue(telescope.focalLength);
	connect(spin, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
	        [this](int val)
	        {
		        telescope.focalLength = val;
		        updateFOVFromTelescope();
	        });
	layoutF->addRow(tr("Focal length (mm)"), spin);

	spin = make_qt_unique<QSpinBox>(*telescopeDialog);
	spin->setMinimum(0);
	spin->setMaximum(180);
	spin->setValue(telescope.eyePieceFOV);
	connect(spin, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
	        [this](int val)
	        {
		        telescope.eyePieceFOV = val;
		        updateFOVFromTelescope();
	        });
	layoutF->addRow(tr("Eye piece FOV (deg)"), spin);

	auto* dspin = make_qt_unique<QDoubleSpinBox>(*telescopeDialog);
	dspin->setMinimum(0);
	dspin->setMaximum(100);
	dspin->setValue(telescope.eyePieceFocalLength);
	connect(dspin,
	        static_cast<void (QDoubleSpinBox::*)(double)>(
	            &QDoubleSpinBox::valueChanged),
	        [this](double val)
	        {
		        telescope.eyePieceFocalLength = val;
		        updateFOVFromTelescope();
	        });
	layoutF->addRow(tr("Eye piece focal length (mm)"), dspin);

	updateFOVFromTelescope();

	toneMappingModel->exposure     = 100.f;
	toneMappingModel->dynamicrange = 10000.f;
	toneMappingModel->autoexposure = true;
	toneMappingModel->purkinje     = true;

	// DBG
	shader = std::make_unique<GLShaderProgram>("default");
	point  = std::make_unique<GLMesh>();
	point->setVertexShaderMapping(*shader, {{"position", 3}});
	point->setVertices({0.f, 0.f, 0.f});
}

void MainWin::updateScene(BasicCamera& camera, QString const& /*pathId*/)
{
	auto& cam = dynamic_cast<OrbitalSystemCamera&>(camera);
	cam.setWindowSize(size());

	if(videomode)
	{
		clock.update(frameTiming);
	}
	else
	{
		clock.update();
	}
	/*if(!clock.drawableFrame())
	{
	    return;
	}*/

	cam.updateUT(clock.getCurrentUt());

	toneMappingModel->autoexposuretimecoeff = clock.getTimeCoeff();

	if(!vrHandler->isEnabled() && cam.getAltitude() < 0.0
	   && cam.target->getOrbitableType() != Orbitable::Type::SPACECRAFT)
	{
		cam.setAltitude(1.0);
	}

	if(!vrHandler->isEnabled())
	{
		CelestialBodyRenderer::overridenScale() = 1.0;
	}

	Controller const* left(vrHandler->getController(Side::LEFT));
	Controller const* right(vrHandler->getController(Side::RIGHT));

	// single grip = translation
	if(leftGripPressed != rightGripPressed)
	{
		Vector3 controllerRelPos;
		if(leftGripPressed && left != nullptr)
		{
			controllerRelPos
			    = Utils::fromQt(cam.seatedTrackedSpaceToWorldTransform()
			                    * left->getPosition())
			          / CelestialBodyRenderer::overridenScale()
			      + cam.relativePosition;
		}
		else if(rightGripPressed && right != nullptr)
		{
			controllerRelPos
			    = Utils::fromQt(cam.seatedTrackedSpaceToWorldTransform()
			                    * right->getPosition())
			          / CelestialBodyRenderer::overridenScale()
			      + cam.relativePosition;
		}
		cam.relativePosition -= controllerRelPos - initControllerRelPos;
	}
	// double grip = scale
	if(leftGripPressed && rightGripPressed && left != nullptr
	   && right != nullptr)
	{
		rescale(initScale
		            * left->getPosition().distanceToPoint(right->getPosition())
		            / initControllersDistance,
		        scaleCenter);
	}

	// apply keyboard controls
	for(unsigned int i(0); i < 3; ++i)
	{
		cam.relativePosition[i]
		    += frameTiming
		       * (cam.getView().inverted()
		          * (negativeVelocity + positiveVelocity))[i]
		       * velMag;
	}

	orbitalSystem->update(clock.getCurrentUt());
	systemRenderer->setUT(clock.getCurrentUt());
	systemRenderer->update(cam);
}

void MainWin::renderScene(BasicCamera const& camera, QString const& /*pathId*/)
{
	auto& cam(renderer.getCamera<OrbitalSystemCamera>("planet"));

	GLHandler::setPointSize(1);
	stars->render(cam.pixelSolidAngle(), BasicCamera::noTrans(cam.getView()));
	systemRenderer->render(camera);
	renderer.renderVRControls();
	// systemRenderer->renderTransparent(camera);
}

void MainWin::renderGui(QSize const& targetSize, AdvancedPainter& painter)
{
	auto const& cam(renderer.getCamera<OrbitalSystemCamera>("planet"));
	std::stringstream stream;
	stream.precision(3);
	// stream << clock.getCurrentFPS() << " FPS" << std::endl;
	stream << round(1.f / frameTiming) << " FPS" << std::endl;
	stream << "Targeting : " << cam.target->getName() << std::endl;
	stream.precision(10);
	stream << "Altitude : " << lengthPrettyPrint(cam.getAltitude()).first << " "
	       << lengthPrettyPrint(cam.getAltitude()).second << std::endl;
	stream.precision(4);
	stream << "UT = " << SimulationTime::UTToStr(clock.getCurrentUt())
	       << std::endl;
	stream.precision(12);
	stream << "Raw UT = " << floor(clock.getCurrentUt() * 10) / 10 << std::endl;
	stream.precision(8);
	stream << "x" << clock.getTimeCoeff()
	       << (clock.getLockedRealTime() ? " (locked)" : "") << std::endl;
	stream << "Velocity : " << lengthPrettyPrint(velMag).first << " "
	       << lengthPrettyPrint(velMag).second << "/s" << std::endl;

	stream << "Exposure " << (toneMappingModel->autoexposure ? "(auto)" : "")
	       << ": " << toneMappingModel->exposure << std::endl;

	if(vrHandler->isEnabled())
	{
		stream.precision(4);
		stream << "Scale : 1 real meter = "
		       << lengthPrettyPrint(1.0
		                            / CelestialBodyRenderer::overridenScale())
		              .first
		       << " "
		       << lengthPrettyPrint(1.0
		                            / CelestialBodyRenderer::overridenScale())
		              .second
		       << std::endl;
	}

	painter.setPen(QPen{Qt::red});
	painter.drawText(
	    QRect{10, 10, targetSize.width() - 10, targetSize.height() - 10},
	    Qt::AlignRight, stream.str().c_str());

	AbstractMainWin::renderGui(targetSize, painter);
}

void MainWin::applyPostProcShaderParams(
    QString const& id, GLShaderProgram const& shader,
    GLFramebufferObject const& currentTarget) const
{
	AbstractMainWin::applyPostProcShaderParams(id, shader, currentTarget);
}

std::vector<GLComputeShader::TextureBinding>
    MainWin::getPostProcessingUniformTextures(
        QString const& id, GLShaderProgram const& shader,
        GLFramebufferObject const& currentTarget) const
{
	auto abstractResult(AbstractMainWin::getPostProcessingUniformTextures(
	    id, shader, currentTarget));
	if(!abstractResult.empty())
	{
		return abstractResult;
	}
	return {};
}

void MainWin::cleanLibraries()
{
	cleanLibrary<LibPlanet>();
	cleanLibrary<trn::LibTerrain>();
}

void MainWin::rescale(double newScale, Vector3 const& scaleCenter)
{
	auto& cam(renderer.getCamera<OrbitalSystemCamera>("planet"));
	Vector3 diff(cam.relativePosition - scaleCenter);
	diff /= newScale / CelestialBodyRenderer::overridenScale();
	cam.relativePosition                    = scaleCenter + diff;
	CelestialBodyRenderer::overridenScale() = newScale;
}

QTreeWidgetItem* MainWin::constructItems(Orbitable const& orbitable,
                                         QTreeWidgetItem* parent)
{
	QTreeWidgetItem* item = nullptr;
	if(parent == nullptr)
	{
		// NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
		item = new QTreeWidgetItem(tree);
	}
	else
	{
		// NOLINTNEXTLINE(cppcoreguidelines-owning-memory)
		item = new QTreeWidgetItem(parent);
	}

	if(orbitable.getOrbitableType() != Orbitable::Type::SPACECRAFT)
	{
		item->setText(0, QString::fromStdString(orbitable.getName()));
	}
	else
	{
		auto const* obt(orbitable.getOrbit());
		if(obt != nullptr)
		{
			const QString beginDate(
			    SimulationTime::utToDateTime(obt->getRange().first)
			        .date()
			        .toString(Qt::ISODate));
			const QString endDate(
			    SimulationTime::utToDateTime(obt->getRange().second)
			        .date()
			        .toString(Qt::ISODate));
			if(obt->isInRange(clock.getCurrentUt()))
			{
				item->setText(0, QString::fromStdString(orbitable.getName())
				                     + "\n(" + beginDate + "\n->" + endDate
				                     + ")");
				item->setForeground(0, QColor("green"));
			}
			else
			{
				item->setText(0, QString::fromStdString(orbitable.getName())
				                     + "\n(" + beginDate + "\n->" + endDate
				                     + ")");
				item->setForeground(0, QColor("red"));
			}
		}
		else
		{
			item->setText(0, QString::fromStdString(orbitable.getName()));
			std::cout << std::string("Spacecraft \"") + orbitable.getName()
			                 + "\" doesn't have an orbit"
			          << std::endl;
		}
	}

	for(auto const& child : orbitable.getChildren())
	{
		item->addChild(constructItems(*child, item));
	}

	return item;
}

std::pair<double, std::string> MainWin::lengthPrettyPrint(double length)
{
	// m
	if(fabs(length) < 500.0)
	{
		return {length, "m"};
	}
	// km
	length /= 1000.0;
	if(fabs(length) < 1495979.0) // 0.01AU
	{
		return {length, "km"};
	}
	// AU
	length /= 149597871.0;
	if(length < 50000.0)
	{
		return {length, "AU"};
	}
	// ly
	length /= 63241.1;
	return {length, "ly"};
}
