#version 150 core

in vec3 position;
in float mag;
in vec3 color;

uniform mat4 camera;
uniform float pixelSolidAngle;
uniform mat4 viewMatrix;

out vec4 f_finalcolor;

void main()
{
	gl_Position = camera * vec4(position, 1.0);
	// lm.m-2
	float irradiance = pow(10.0, 0.4 * (-mag - 14.0));

	// alpha := angle between vertex and camera view dir
	vec3 posInViewSpace = normalize(vec3(viewMatrix * vec4(position, 1.0)));
	float cosAlpha = -posInViewSpace.z;
	float mul    = irradiance / (pixelSolidAngle * pow(cosAlpha, 6.0));
	f_finalcolor = vec4(mul * color, 1.0);
}
